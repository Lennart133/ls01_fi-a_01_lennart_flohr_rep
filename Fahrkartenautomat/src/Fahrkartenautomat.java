﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      
    	
    	while(true) {
    	zuZahlenderBetrag = fahrkartenbestellung();
    	
    	
    	eingezahlterGesamtbetrag = fahrkartenBezahlen();//Bezhalen der Fahrkarte
       
    	fahrkartenAusgeben();//Fahrkarten ausgabe
    	
    	rückgabebetrag = rueckgeldAusgeben();//Rückgeld ausgabe
      

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n");
    	}
    }
    static Scanner tastatur = new Scanner(System.in);
    static double zuZahlenderBetrag; // Kosten
    static double eingezahlterGesamtbetrag; //v Betrag der eingezahlt wurde
    static double eingeworfeneMünze;// aufnahme der eingeworfenen Münzen
    static double rückgabebetrag;//  Rückgeld
    static byte AnzahlderTickets = 0; 
    
    public static double fahrkartenbestellung() {  	// Auswahl der Fahrkarte
    	
    	
    	
    	 	  
          System.out.print("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + 
          		"          Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + 
          		"          Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n ");
         int auswahl = tastatur.nextInt();
         
         switch(auswahl){ //überprüft die Fahrkarte
         case 1:
        	 System.out.println("Bitte geben Sie ein wie viele Tickets Sie kaufen wollen");
         System.out.println("Die Ticket Anzahl darf nur zwischen 1 und 10 liegen");
    	   System.out.print("Anzahl der Tickets : "); 
       AnzahlderTickets = tastatur.nextByte();
       
       while((AnzahlderTickets <= 0) || (AnzahlderTickets > 10)) {
    	   System.out.println("Bitte geben Sie ein wie viele Tickets Sie kaufen wollen");
       System.out.println("Die Ticket Anzahl darf nur zwischen 1 und 10 liegen");
    	   System.out.print("Anzahl der Tickets : "); 
       AnzahlderTickets = tastatur.nextByte();
       }zuZahlenderBetrag = 2.90 * AnzahlderTickets;
       break;
       case 2:
    	   System.out.println("Bitte geben Sie ein wie viele Tickets Sie kaufen wollen");
       System.out.println("Die Ticket Anzahl darf nur zwischen 1 und 10 liegen");
  	   System.out.print("Anzahl der Tickets : "); 
     AnzahlderTickets = tastatur.nextByte();
     
     while((AnzahlderTickets <= 0) || (AnzahlderTickets > 10)) {
  	   System.out.println("Bitte geben Sie ein wie viele Tickets Sie kaufen wollen");
     System.out.println("Die Ticket Anzahl darf nur zwischen 1 und 10 liegen");
  	   System.out.print("Anzahl der Tickets : "); 
     AnzahlderTickets = tastatur.nextByte();
     }zuZahlenderBetrag = 8.60 * AnzahlderTickets;
     break;
     
       case 3:
    	   System.out.println("Bitte geben Sie ein wie viele Tickets Sie kaufen wollen");
           System.out.println("Die Ticket Anzahl darf nur zwischen 1 und 10 liegen");
        	   System.out.print("Anzahl der Tickets : "); 
           AnzahlderTickets = tastatur.nextByte();
           
           while((AnzahlderTickets <= 0) || (AnzahlderTickets > 10)) {
        	   System.out.println("Bitte geben Sie ein wie viele Tickets Sie kaufen wollen");
           System.out.println("Die Ticket Anzahl darf nur zwischen 1 und 10 liegen");
        	   System.out.print("Anzahl der Tickets : "); 
           AnzahlderTickets = tastatur.nextByte();
         }zuZahlenderBetrag = 23.50 * AnzahlderTickets;
           break;
           default:// nur wenn ein Falscher wert angegeben wird
        	   zuZahlenderBetrag = 0;
         } 
         return zuZahlenderBetrag;//Der Betrag wird ausgegeben
    } 
    
    
    public static double fahrkartenBezahlen() {// bezahlen der Fahrkarte
    	 
    	eingezahlterGesamtbetrag = 0.0;
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
      	   if(eingeworfeneMünze <= 2 && eingeworfeneMünze >= 0.5){
      	   
             eingezahlterGesamtbetrag += eingeworfeneMünze;
             }
             else {
            	 System.out.println("Sie dürfen nur mind. 5Ct, höchstens 2 Euro einwerfen ");
      	   }
         }
         return eingezahlterGesamtbetrag;
    }
    
    
          public static void fahrkartenAusgeben()  {// Fahrkarten ausgabe
        	  
        	  System.out.println("\nFahrschein wird ausgegeben");
              for (int i = 0; i < 8; i++)
              
                 System.out.print("=");
                 try {
       			Thread.sleep(250);
       		} catch (InterruptedException e) {
       			e.printStackTrace();
       		}
              }
              System.out.println("\n\n");
        	  
          }
          
          
          
          public static double rueckgeldAusgeben() { //Rückgeldausgabe
        	  rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
              if(rückgabebetrag > 0.0)
              {
           	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
           	   System.out.println("wird in folgenden Münzen ausgezahlt:");
  
                  while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
                  {
               	  System.out.println("2 EURO");
       	          rückgabebetrag -= 2.0;
                  }
                  while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
                  {
               	  System.out.println("1 EURO");
       	          rückgabebetrag -= 1.0;
                  }
                  while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
                  {
               	  System.out.println("50 CENT");
       	          rückgabebetrag -= 0.5;
                  }
                  while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
                  {
               	  System.out.println("20 CENT");
        	          rückgabebetrag -= 0.2;
                  }
                  while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
                  {
               	  System.out.println("10 CENT");
       	          rückgabebetrag -= 0.1;
                  }
                  while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
                  {
               	  System.out.println("5 CENT");
        	          rückgabebetrag -= 0.05;
                  }
              } 
              return rückgabebetrag;
          }
}