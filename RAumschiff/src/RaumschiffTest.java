
public class RaumschiffTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Raumschiff Klingon = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
Ladung ferengiSchneckensaft = new Ladung("Ferengi Schenckensaft", 200);
Ladung Schwerter = new Ladung("Bat'leth Klingonen Schwert", 200);

Raumschiff Remulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
Ladung BorgSchrott = new Ladung("Borg Schrott", 5);
Ladung RoteMaterie = new Ladung("Rote Materie", 2);
Ladung PlasmaWaffen = new Ladung("Plasma Waffen", 50);

Raumschiff Vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni Var");
Ladung Forschungsonde = new Ladung("Forschungssonde", 35);
Ladung Photonentorpedo = new Ladung("Photonentorpedo", 3);
//Ladung Hinzufügen
Klingon.addLadung(ferengiSchneckensaft);
Klingon.addLadung(Schwerter);

Remulaner.addLadung(BorgSchrott);
Remulaner.addLadung(RoteMaterie);
Remulaner.addLadung(PlasmaWaffen);

Vulkanier.addLadung(Forschungsonde);
Vulkanier.addLadung(Photonentorpedo);

Klingon.photonentorpedosAbschießen(Remulaner);
Remulaner.phaserkanonenAbschießen(Klingon);
Vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
Klingon.zustandRaumschiff();
Klingon.ladungAusgeben();


Klingon.photonentorpedosAbschießen(Remulaner);
Klingon.photonentorpedosAbschießen(Remulaner);

Klingon.zustandRaumschiff();
Klingon.ladungAusgeben();
Klingon.brodcastkommunikator();
Remulaner.zustandRaumschiff();
Remulaner.ladungAusgeben();
Remulaner.brodcastkommunikator();
Vulkanier.zustandRaumschiff();
Vulkanier.ladungAusgeben();
Vulkanier.brodcastkommunikator();

System.out.println(Raumschiff.einträgezurückgeben());


	}

}
