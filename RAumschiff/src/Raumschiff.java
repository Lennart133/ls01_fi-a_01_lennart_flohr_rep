import java.util.ArrayList;

/**
 * Die Klasse moderiert das Raumschiff 
 * @author Lennart Flohr
 * @version 1.2
 *
 */
// Klasse 
public class Raumschiff {
//Attribute

	private int photonentorpedoMenge;
	private int energieversorgung;
	private int schildProzent;
	private int hülleProzent;
	private int lebenserhaltungProzent;
	private int androidanzahl;
	private String schiffsname;
	private static ArrayList<String> brodcastkommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichns = new ArrayList<Ladung>();
	
	

	/**
	 * Parameterlose Konstruktoren der Klasse Raumschiffe
	 */
	Raumschiff() {
	}
/**
 * Alle Zustände des Raumschiffes auf der Konsole mit entsprechenden Namen ausgeben.
 * @param photonentorpedoMenge ist die menge der Photonentorpedos die dem Raumschiff zur verfügung stehen 
 * @param energieversorgung gibt an wie viel Prozent die Energieversorgung noch hat
 * @param schildProzent gibt an wie viel Prozent die Schilde noch haben
 * @param hülleProzent gibt an wie viel Prozent die Hülle noch hat
 * @param lebenserhaltungProzent gibt an wie viel Prozent die Lebenserhaltung noch hat
 * @param androidanzahl gibt an wie viel Androiden jedes Raumschiff besitzt
 * @param schiffsname gibt den Namen des Schiffes an
 */
	Raumschiff(int photonentorpedoMenge, int energieversorgung, int schildProzent, int hülleProzent,
			int lebenserhaltungProzent, int androidanzahl, String schiffsname) {
		this.photonentorpedoMenge = photonentorpedoMenge;
		this.energieversorgung = energieversorgung;
		this.schildProzent = schildProzent;
		this.hülleProzent = hülleProzent;
		this.lebenserhaltungProzent = lebenserhaltungProzent;
		this.androidanzahl = androidanzahl;
		this.schiffsname = schiffsname;
		
	}

	// getter und seter
	public int getPhotonentorpedMenge() {
		return photonentorpedoMenge;
	}

	public void setPhotonentorpedMenge(int photonentorpedoMenge) {
		this.photonentorpedoMenge = photonentorpedoMenge;
	}

	public int getEnergieversorgung() {
		return energieversorgung;
	}

	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public int getSchildProzent() {
		return schildProzent;
	}

	public void setSchildProzent(int schildProzent) {
		this.schildProzent = schildProzent;
	}

	public int getHülleProzent() {
		return hülleProzent;
	}

	public void setHülleProzent(int hülleProzent) {
		this.hülleProzent = hülleProzent;
	}

	public int getLebenserhaltungProzent() {
		return lebenserhaltungProzent;
	}

	public void setLebenserhaltungProzent(int lebenserhaltungProzent) {
		this.lebenserhaltungProzent = lebenserhaltungProzent;
	}

	public int getAndroidanzahl() {
		return androidanzahl;
	}

	public void setAndroidanzahl(int androidanzahl) {
		this.androidanzahl = androidanzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
/**
 * 
 * @param neueLadung lässt eine neue Ladung für das Raumschiff zu 
 */
	// Ladungsverzeuchnis
	public void addLadung(Ladung neueLadung) {
	}

	// Ausgabe für alle Zustände
	/**
	 *  Hier werden nun die Zuständne für des Raumschiffes ausgegeben 
	 */
	public void zustandRaumschiff() {
		System.out.println("Wir haben " + photonentorpedoMenge + " Torpedos übrig, Captain");
		System.out.println("Wir haben " + energieversorgung + "% an Energie");
		System.out.println("Wir haben " + schildProzent + "% an Schildleistung");
		System.out.println("Wir haben " + hülleProzent + "% der Hülle");
		System.out.println("Wir haben " + lebenserhaltungProzent + "% am leben");
		System.out.println("Wir haben " + androidanzahl + " Androiden");
	}
/**
 * 
 * @param nachrichten Hier wird die Nachricht für die Raumschiffe im Brodcastkommunikator ausgegeben
 */
	public void nachrichtAnAlle(String nachrichten) {
		brodcastkommunikator.add("\n" + this.schiffsname + ":"+ nachrichten);
	}
/**
 * 
 * @return Der Brodcastkommunikator wird zurückgegeben
 */
	public static ArrayList<String> einträgezurückgeben() {
		return brodcastkommunikator;
	}
/**
 * 
 * @param photonentorpedosAbschießen schießt die Kanonen ab 
 * danach wird die Meldung "Photonentorpedo abgeschossen" ausgegeben 
 * die Menge der Torpedos wird um eins herunter gesetzt 
 */
	public void photonentorpedosAbschießen(Raumschiff r) {
		if (photonentorpedoMenge > 0) {
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			photonentorpedoMenge--;
			treffervermerken(r);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}
/**
 * Die Ladung wird ausgegeben
 */
	public void ladungAusgeben() {
		for (int a = 0; a < ladungsverzeichns.size(); a++) {
			System.out.println("ich habe " + ladungsverzeichns.get(a).getName() + " als Ladung");
			System.out.println("Davon " + ladungsverzeichns.get(a).getMenge() + "Stück");
		}
	}
/**
 * 
 * wenn die Energieversorgung unter 50% ist kann die kanone nicht abgeschossen werden
 * in dem Fall wird die Nachicht click ausgegeben 
 */
	public void phaserkanonenAbschießen(Raumschiff r) {
		if (energieversorgung < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffervermerken(r);
		}
	}
/**
 * 
 * bei einem Treffer wird der Schiffsname ausgegeben mit der Meldung "wird getroffen"
 * bei einem Treffer werden die Schilde um 50% verringert 
 * wenn die Schilde bei 0% sind werden 50 % von der Hülle abgezogen 
 * wenn die Hülle bei 0% ist wird die Lebenserhaltung auf 0 gesetzt
 */
	private void treffervermerken(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen!");
		r.setSchildProzent(r.getSchildProzent() - 50);
		if (r.getSchildProzent() <= 0) {
			r.setHülleProzent(r.getHülleProzent() - 50);
			r.setEnergieversorgung(r.getEnergieversorgung() - 50);
			if (r.getHülleProzent() <= 0) {
				r.setLebenserhaltungProzent(0);
				nachrichtAnAlle("Alle Lebenserhaltungs wurden vernichtet.");
			}
		}
	}
/**
 * der Brodcastkommunikator gibt die erste Nachicht aus
 */
	public void brodcastkommunikator() {
		for (int a = 0; a < brodcastkommunikator.size(); a++) {
			System.out.println("Erste Nachricht " + brodcastkommunikator.get(a));
		}
	}
}
