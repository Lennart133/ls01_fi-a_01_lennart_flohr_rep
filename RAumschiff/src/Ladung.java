/**
 * Die Klasse modelliert die Ladung
 * @author Lennart Flohr
 *@version 1.2
 */
//Klasse
public class Ladung {
/** 
 * @param name gibt den namen der Ladung an
 * @param menge gibt die Menge der Ladung an
 */
	
	private String name;
	private int menge ;
	
	Ladung(){
	}
	Ladung(String name, int menge ){
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
}
